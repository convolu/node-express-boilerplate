"use strict";

var app = require("./app");

function server() {
    var ser = app.listen(3000, function () {
        console.log('App listening at 3000');
    });

    return ser;
}

module.exports = server;
