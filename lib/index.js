"use strict";

var config = require("./config"),
    cluster = require("cluster"),
    numCPUs = require("os").cpus().length,
    process = require("process"),
    server = require("./server"),
    workerNo = parseInt(process.env.WORKERS, 10),
    workerLimit, i;

if (isNaN(workerNo) || workerNo > numCPUs || workerNo < 1) {
    workerLimit = numCPUs;
} else {
    workerLimit = workerNo;
}

if (cluster.isMaster) {
    for (i = 0; i < workerLimit; i++) {
        cluster.fork();
    }

    cluster.on('online', function (worker) {
        console.log('worker with pid %s created', worker.process.pid);
    });

    cluster.on('exit', function (worker, code, signal) {
        console.log('worker ' + worker.process.pid + 'died');
    });
} else {
    server();
}
