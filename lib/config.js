"use strict";

var dotenv = require("dotenv"),
    process = require("process"),

    config;

console.log('NODE_ENV=%s', process.env.NODE_ENV);

config = function () {
    var filename = '';
    switch (process.env.NODE_ENV){
        case 'production':
            dotenv.load({"path": "../config/production.env"});
            break;
        case 'test':
        case 'testing':
            dotenv.load({"path": "../config/testing.env"});
            break;
    }
};

config();
console.log("Loaded conf");
